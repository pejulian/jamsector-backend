/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  },

  // ==========================================
  // AuthController
  // ==========================================
  'get /api/auth/login': 'AuthController.login',
  'get /api/auth/logout': 'AuthController.logout',
  'get /api/auth/local': 'AuthController.callback',
  'get /api/auth/local/:action': 'AuthController.callback', 

  // actually we don't need this code below for this tutorial
  // 'get /auth/:provider': 'AuthController.provider',
  // 'get /auth/:provider/callback': 'AuthController.callback',
  // 'get /auth/:provider/:action': 'AuthController.callback', 

  // ==========================================
  // BandController
  // ==========================================
  'get /api/bands': 'BandController.list',
  'get /api/bands/:id': 'BandController.details',      
  'post /api/bands/create': 'BandController.create',
  'get /api/bands/:id/:action': 'BandController.action',

  // ==========================================
  // ImageController
  // ==========================================
  'post /api/images/upload': 'ImageController.images',
  'get /api/images/:id': 'ImageController.images',

  // ==========================================
  // AlbumController
  // ==========================================
  'get /api/albums?band=:band&genre=:genre&year=:year&language=:language': 'AlbumController.list',
  'get /api/albums/:id': 'AlbumController.details', 
  'post /api/albums/create': 'AlbumController.create',

  // ==========================================
  // EventController
  // ==========================================
  'get /api/events?band=:band&genre=:genre&venue=:venue': 'EventController.list',
  'get /api/events/:id': 'EventController.details',
  'post /api/events/create': 'EventController.create',

  // ==========================================
  // VenueController
  // ==========================================
  'get /api/venues': 'VenueController.list',
  'get /api/venues/:id': 'VenueController.details',
  'post /api/venues/create': 'VenueController.create'

};

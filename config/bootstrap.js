/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

var async = require('async');

module.exports.bootstrap = function(cb) {

  // Load passport strategies
  sails.services.passport.loadStrategies();

  var actions = {

    // Create the superuser if it doesnt exist
    superuser: function( callback ) {
      sails.log.debug('jamsector.bootstrap: creating superuser ...');
      UserService.create({
        email: 'superuser@jamsector.com',
        username: 'superuser',
        password: 'P@$$w0Rd',
        account: 'superuser',
        callback: function( err, user ) {
          if ( err ) {
            sails.log.error('jamsector.bootstrap:', err );
            return callback( err, null )
          }
          else {
            return callback( null, user );
          }
        }
      });
    },
    
    // Load default keywords if they dont exist.
    // [1] 
    // keywords: function( user, callback ) {
    // }


  };

  async.parallel( actions, function( err, superuser, keywords ) {

  });



  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};

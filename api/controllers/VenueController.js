/**
 * VenueController
 *
 * @description :: Server-side logic for managing Venues
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var lodash = require('lodash'),
    async = require('async');

module.exports = {

  // List events with pagination and filter support
  list: function( req, res, next ) {
    if ( req.method.toUpperCase() === 'GET' ) {
      
    }
    else {
      return res.json({
        status: 403,
        message: 'Action not allowed!',
        data: null
      });
    }
  },

  details: function( req, res, next ) {

  },

  // Create or update
  create: function( req, res, next ) {
    if ( req.method.toUpperCase() === 'POST' && req.session.authenticated ) {

    }
    else {
      return res.json({
        status: ( req.session.authenticated ? 403 : 401 ),
        message: 'error',
        data: ( req.session.authenticated ? 'Action not allowed!' : 'Unauthorized action!')
      });
    }
  },

  remove: function( req, res, next ) {
    if ( req.method.toUpperCase() === 'POST' && req.session.authenticated ) {

    }
    else {
      return res.json({
        status: ( req.session.authenticated ? 403 : 401 ),
        message: 'error',
        data: ( req.session.authenticated ? 'Action not allowed!' : 'Unauthorized action!')
      });
    }
  },




}
/**
 * EventController
 *
 * @description :: Server-side logic for managing Events
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var lodash = require('lodash'),
    async = require('async');

module.exports = {

  // List events with pagination and filter support
  list: function( req, res, next ) {
    if ( req.method.toUpperCase() === 'GET' ) {
      
    }
    else {
      return res.json({
        status: 403,
        message: 'Action not allowed!',
        data: null
      });
    }
  },

  // Create or update
  create: function( req, res, next ) {
    if ( req.method.toUpperCase() === 'POST' && req.session.authenticated ) {

      // An event must have:
      // name
      // start and end date and time (or all day; no time)
      // venue (owner)
      // description (optional)
      // location (place)
      // event cover photo (optional)
      // bands involved (optional)
      // price (optional)
      // photos (optional)

    }
    else {
      return res.json({
        status: ( req.session.authenticated ? 403 : 401 ),
        message: 'error',
        data: ( req.session.authenticated ? 'Action not allowed!' : 'Unauthorized action!')
      });
    }
  },

  details: function( req, res, next ) {

  },

  // Remove an event
  remove: function( req, res, next ) {
    if ( req.method.toUpperCase() === 'POST' && req.session.authenticated ) {

    }
    else {
      return res.json({
        status: ( req.session.authenticated ? 403 : 401 ),
        message: 'error',
        data: ( req.session.authenticated ? 'Action not allowed!' : 'Unauthorized action!')
      });
    }
  },




}
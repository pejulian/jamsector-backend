/**
 * KeywordController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var lodash = require('lodash');

var KeywordController = {

  keywords: function( req, res, next ) {

    var filter = req.param('filter');

    if ( lodash.isEmpty( filter ) || lodash.isUndefined( filter ) || lodash.isNull( filter ) ) {
      return res.json({
        status: 500,
        message: 'error',
        data: 'filter not specified, unable to fetch keywords'
      })
    }

    Keyword
      .find()
      .where({ filter: filter } )
      .exec( function( err, keywords ) {
        if ( err )
          return res.json({
            status: 500,
            message: 'error',
            data: err
          });
        return res.json({
          status: 200,
          message: 'success',
          data: keywords
        });
      });

  }
};

module.exports = KeywordController;

/**
 * Authentication Controller
 *
 * This is merely meant as an example of how your Authentication controller
 * should look. It currently includes the minimum amount of functionality for
 * the basics of Passport.js to work.
 */

var lodash = require('lodash');

var AuthController = {

  login: function ( req, res ) {
    var strategies = sails.config.passport, 
        providers  = {};

    // Get a list of available providers for use in your templates.
    Object.keys(strategies).forEach(function (key) {
      if (key === 'local') {
        return;
      }

      providers[key] = {
        name: strategies[key].name, 
        slug: key
      };
    });

    res.json({
      status: 200,
      message: 'Login strategies',
      data: providers
    });
  },

  /**
   * Log out a user and return them to the homepage
   *
   * Passport exposes a logout() function on req (also aliased as logOut()) that
   * can be called from any route handler which needs to terminate a login
   * session. Invoking logout() will remove the req.user property and clear the
   * login session (if any).
   *
   * For more information on logging out users in Passport.js, check out:
   * http://passportjs.org/guide/logout/
   *
   * @param {Object} req
   * @param {Object} res
   */
  logout: function (req, res) {
    req.logout();
    
    // mark the user as logged out for auth purposes
    req.session.authenticated = false;
    
    res.json({
      status: 200,
      message: 'User logged out',
      data: null
    });
  },


  /**
   * Create a third-party authentication endpoint
   *
   * @param {Object} req
   * @param {Object} res
   */
  provider: function (req, res) {
    passport.endpoint(req, res);
  },

  /**
   * Create a authentication callback endpoint
   *
   * This endpoint handles everything related to creating and verifying Pass-
   * ports and users, both locally and from third-aprty providers.
   *
   * Passport exposes a login() function on req (also aliased as logIn()) that
   * can be used to establish a login session. When the login operation
   * completes, user will be assigned to req.user.
   *
   * For more information on logging in users in Passport.js, check out:
   * http://passportjs.org/guide/login/
   *
   * @param {Object} req
   * @param {Object} res
   */
  callback: function ( req, res ) {

    if ( req.method.toUpperCase() === 'GET' ) {
      passport.callback( req, res, function ( err, user ) {

        if ( err ) {
          return res.json({
            status: 500,
            message: ( lodash.isString( err ) ? err : 'error' ),
            data: ( user ? user : err )
          });
        }

        if ( !req.session.authenticated ) {
          req.login( user, function ( err ) {

            if ( err ) {
              return res.json({
                status: 500,
                message: 'Authentication failed',
                data: err
              });
            }
            
            // Mark the session as authenticated to work with default Sails sessionAuth.js policy
            req.session.authenticated = true;
            
            // Upon successful login, send the user to the homepage were req.user
            // will be available.
            return res.json({
              status: 200,
              message: 'User authenticated!',
              data: user
            });

          });
        }
        else {
          return res.json({
            status: 200,
            message: 'User is authenticated!',
            data: user
          });
        }
      });
    }
    else {
      return res.json({
        status: 403,
        message: 'error',
        data: 'Action not allowed!'
      });
    }
  },

  /**
   * Disconnect a passport from a user
   *
   * @param {Object} req
   * @param {Object} res
   */
  disconnect: function (req, res) {
    passport.disconnect(req, res);
  }
};

module.exports = AuthController;

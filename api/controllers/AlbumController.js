/**
 * AlbumController
 *
 * @description :: Server-side logic for managing Albums
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var lodash = require('lodash'),
    passport = require('passport'),
    async = require('async'),
    skipper = require('skipper-gridfs'),
    moment = require('moment'),
    mime = require('mime');

 var AlbumController = {

  // Can list albums by band, genre, year, language
  list: function( req, res, next ) {

    if ( req.method.toUpperCase() === 'GET' ) {

      var band = req.param('band') || 'ALL',
          genre = req.param('genre') || 'ALL',
          year = req.param('year') || 'ALL',
          language = req.param('language') || 'ALL',
          page = req.param('page') || 0;

      var query = {};
      if ( !lodash.isEqual( band, 'ALL') )
        query.band = band;
      if ( !lodash.isEqual( genre, 'ALL') )
        query.genre = genre;
      if ( !lodash.isEqual( year, 'ALL') )
        query.year = year;
      if ( !lodash.isEqual( language, 'ALL') )
        query.language = language;

      if ( !lodash.isNaN( page ) ) 
        page = parseInt( page, 10 );

      Album
        .find( query )
        .paginate({ page: page, limit: 10 })
        .exec( function( err, albums ) {

          if ( err )
            return res.json({
              status: 500,
              message: 'error',
              data: err
            });

          return res.json({
            status: 200,
            message: 'success',
            data: albums
          });
        });
    }
    else {
      return res.json({
        status: 403,
        message: 'Action not allowed!',
        data: null
      });
    }
  },

  // Get album details
  details: function( req, res, next ) {

    // Get band information
    if ( req.method.toUpperCase() === 'GET' ) {

      var id = req.param('id'),
          actions = [
            function( cb ) {
              Album.findOne({ id: id })
                .populate('photos')
                .exec( function( err, album ) {
                  if ( err ) {
                    return cb( err );
                  }
                  else {
                    if ( !album ) {
                      sails.log.error('Could not find album with id', id );
                      return cb( true );                    
                    }
                    else {
                      delete album.avatar.createdAt;
                      delete album.avatar.updatedAt;
                      album.cover.url = lodash.template('/api/images/${id}')({ id: band.avatar });
                      return cb ( null, album );
                    }
                  }
                });
            }
          ];

      async.waterfall( actions, function( err, album ) {
        if ( err ) {
          return res.json({
            status: 500,
            message: 'error',
            data: err
          });
        }
        else {
          return res.json({
            status: 200,
            message: 'success',
            data: album
          });
        }
      });
    }
    else {
      return res.json({
        status: 403,
        message: 'Action not allowed!',
        data: null
      });
    }
  },

  create: function( req, res, next ) {

    if ( req.method.toUpperCase() === 'POST' && req.session.authenticated ) {

      var name = req.param('name'),
          band = req.param('band'),
          arranger = req.param('arranger'),
          language = req.param('language'),
          producer = req.param('producer'),
          nationality = req.param('nationality'),
          label = req.param('label'),
          type = req.param('type'),
          genre = req.param('genre'),
          date = req.param('date'),
          cover = req.file('cover');

      if ( !moment( date, "YYYY-MM-DDTHH:mm:ss", true).isValid() ) 
        return res.json({
          status: 500,
          message: 'error',
          data: 'Invalid date format passed (dates must be in ISO8601 format)'
        });

      if ( lodash.isEmpty( name ) || lodash.isEmpty( band ) || lodash.isEmpty( language )
            || lodash.isEmpty( genre ) )
        return res.json({
          status:500,
          message: 'error',
          data: 'One or more required parameters are empty'
        });

      var actions = [

        // Create the album
        function( cb ) {

          var query = { 
            name: name, 
            band: band, 
            language: language, 
            genre: genre, 
            date: date 
          };
          
          if ( !lodash.isEmpty( arranger ) )
            query.arranger = arranger;
          if ( !lodash.isEmpty( producer ) )
            query.producer = producer;
          if ( !lodash.isEmpty( nationality ) )
            query.nationality = nationality;
          if ( !lodash.isEmpty( label ) )
            query.label = label;
          if ( !lodash.isEmpty( type ) ) 
            query.type = type;

          Album.create( query ).exec( function( err, album ) {
            if ( err ) {
                if ( err.ValidationError )
                  err = validation( Album, err.ValidationError );

                KeywordService.store({
                  keyword: genre,
                  filter: 'genre'
                });

                return cb( err );
              }
              else
                return cb( null, album );
          });
        },

        // Create the album cover and associate it to the album
        function( album, cb ) {
          ImageService.upload({ 
            image: cover,
            identifier: album,
            type: 'cover',
            callback: function( err, results ) {
              if ( err ) {
                if ( err.ValidationError )
                  err = validation( Image, err.ValidationError );
                return cb( err );
              }
              else {
                if ( lodash.isEmpty( results ) ) {
                  Album
                    .destroy({ id: album.id })
                    .exec( function( destroyErr ) {
                      return cb( destroyErr || 'Album cover could not be uploaded', null, null );
                    });
                }
                else
                  return cb( null, results, album );
              }
            }
          });
        },

        // Associate the cover image to the album
        function( images, album, cb ) {

          var image = images[0];
          Album.update({ id: album.id }, { cover: image.id }, function( err, albums ) {
            if ( err )
              return cb( err );
            else {
              return cb( null, image, albums[0] );
            }
          });
        }
      ];

      async.waterfall( actions, function( err, image, album ) {
        if ( err ) {
          return res.json({
            status: 500,
            message: 'error',
            data: err
          });
        }
        else {
          return res.json({
            status: 200,
            message: 'success',
            data: album
          });
        }
      });
    }
    else {
      return res.json({
        status: ( req.session.authenticated ? 403 : 401 ),
        message: 'error',
        data: ( req.session.authenticated ? 'Action not allowed!' : 'Unauthorized action!')
      });
    }
  }
 };

 module.exports = AlbumController;
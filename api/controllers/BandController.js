/**
 * BandController
 *
 * @description :: Server-side logic for managing Bands
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var lodash = require('lodash'),
		passport = require('passport'),
		async = require('async'),
		moment = require('moment');

module.exports = {

	list: function( req, res, next ) {

		if ( req.method.toUpperCase() === 'GET' ) {
			
			var page = req.param('page') || 0;
			if ( !lodash.isNaN( page ) ) 
				page = parseInt( page, 10 );

			Band.find()
				.paginate({ page: page, limit: 10 })
				.exec( function( err, bands ) {
					if ( err ) {
						return res.json({
							status: 500,
							message: 'Error listing bands!',
							data: err
						});
					}
					else {
						return res.json({
							status: 200,
							message: 'success',
							data: bands
						});
					}
				});
		}
		else {
			return res.json({
				status: 403,
				message: 'Action not allowed!',
				data: null
			});
		}
	},

	details: function( req, res, next ) {

		// Get band information
		if ( req.method.toUpperCase() === 'GET' ) {

			var id = req.param('id'),
					actions = [
						function( cb ) {
							Band.findOne({ id: id })
								.populate('pictures')
								.exec( function( err, band ) {
									if ( err ) {
										return cb( err );
									}
									else {
										if ( !band ) {
											sails.log.error('Could not find band with id', id );
											return cb( true );										
										}
										else {
											delete band.picture.createdAt;
											delete band.picture.updatedAt;
											band.picture.url = lodash.template('/api/images/${id}')({ id: band.picture });
											return cb ( null, band );
										}
									}
								});
						}
					];

			async.waterfall( actions, function( err, band ) {
				if ( err ) {
					return res.json({
						status: 500,
						message: 'error',
						data: err
					});
				}
				else {
					return res.json({
						status: 200,
						message: 'success',
						data: band
					});
				}
			});
		}
		else {
			return res.json({
				status: 403,
				message: 'Action not allowed!',
				data: null
			});
		}
	},

	create: function( req, res, next ) {

		if ( req.method.toUpperCase() === 'POST' && req.session.authenticated ) {

			var picture = req.file('picture'),
					name = req.param('name'),
					email = req.param('email'),
					phone = req.param('phone'),
					address = req.param('address'),
					about = req.param('about'),
					formed = req.param('formed');

			if ( !moment( formed ).isValid() ) 
        return res.json({
          status: 500,
          message: 'error',
          data: 'Invalid date format passed (dates must be in ISO8601 format)'
        });

			var actions = [

				// Create the band
				function( cb ) {
					Band
						.create({
							name: name,
							email: email,
							phone: phone,
							address: address,
							about: about,
							formed: formed
						})
						.exec( function( err, band ) {
							if ( err ) {
								if ( err.ValidationError )
									err = validation( Band, err.ValidationError );
								return cb( err );
							}
							else
								return cb( null, band );
						});
				},

				// Create the picture and associate the it to the band
				function( band, cb ) {
					ImageService.upload({ 
						image: picture,
						identifier: band,
						type: 'picture',
						callback: function( err, results ) {
							if ( err ) {
								if ( err.ValidationError )
									err = validation( Image, err.ValidationError );
								return cb( err );
							}
							else {
								if ( lodash.isEmpty( results ) ) {
									Band
										.destroy({ id: band.id })
										.exec( function( destroyErr ) {
											return cb( destroyErr || 'Band picture could not be uploaded', null, null );
										});
								}
								else
									return cb( null, band, results );
							}
						}
					});
				},

				// Associate the band to the picture
				function( band, results, cb ) {

					var image = results[0];
					Band.update({ id: band.id }, { picture: image.id }, function( err, band ) {
						if ( err )
							return cb( err );
						else {
							return cb( null, band[0], image );
						}
					});
				}
			];

			async.waterfall( actions, function( err, band, results ) {
				
				if ( err ) {
					return res.json({
						status: 500,
						message: 'An error occurred while creating this band!',
						data: err
					});
				}
				else {
					return res.json({
						status: 200,
						message: 'success',
						data: band
					});
				}
			});
		}
		else {
			return res.json({
				status: ( req.session.authenticated ? 403 : 401 ),
				message: 'error',
				data: ( req.session.authenticated ? 'Action not allowed!' : 'Unauthorized action!')
			});
		}
	},

	// like/unlike, subscribe/unsubscribe
	action: function( req, res, next ) {
		if ( req.method.toUpperCase() === 'GET' && req.session.authenticated ) {

			var id = req.param('id'),
					action = req.param('action'),
					user = req.session.passport.user;

			if ( lodash.isEmpty( action ) || lodash.isUndefined( action ) || lodash.isNull( action ) )
				return res.json({
					status: 500,
					message: 'error',
					data: 'required input action is missing!'
				});

			// Attempt to find the given activity
			Activity
				.findOne({
					user: user,
					band: id,
					action: action
				})
				.exec( function( err, activity ) {
					if ( err )
						return res.json({
							status: 500,
							message: 'error',
							data: err
						});

					// If activity does not exist, create it
					if ( lodash.isEmpty( activity ) || lodash.isUndefined( activity ) || lodash.isNull( activity ) ) {
						Activity
							.create({ 
								user: user, 
								band: id, 
								action: action 
							})
							.exec( function( err, activity ) {
								if ( err )
									return res.json({
										status: 500,
										message: 'error',
										data: err
									});
								return res.json({
									status: 200,
									message: 'success',
									data: action + ' applied'
								});
							});
					}
					// Activity exists, delete it
					else {
						Activity
							.destroy({
								user: user,
								band: id,
								action: action 
							})
							.exec( function( destroyErr ) {
								if ( destroyErr )
										return res.json({
											status: 500,
											message: 'error',
											data: destroyErr
										});

								return res.json({
									status: 200,
									message: 'success',
									data: action + ' removed'
								});
							});
					}
				});
		}
		else {
			return res.json({
				status: ( req.session.authenticated ? 403 : 401 ),
				message: 'error',
				data: ( req.session.authenticated ? 'Action not allowed!' : 'Unauthorized action!')
			});
		}
	}
	
};


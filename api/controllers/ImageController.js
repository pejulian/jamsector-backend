/**
 * BandController
 *
 * @description :: Server-side logic for managing Bands
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var lodash = require('lodash'),
    mime = require('mime'),
    async = require('async');

module.exports = {

  images: function( req, res, next ) {

    // Send image id to view image and an optional download param to download the picture
    if ( req.method.toUpperCase() === 'GET' && req.session.authenticated ) {

      var id = req.param('id'),
          download = req.param('download') || false,
          actions = [
            function( cb ) {
              Image.findOne( id ).exec( function( err, image ) {
                if ( err ) {
                  sails.log.error('image with id', id, 'not found')
                  return cb( err );
                }
                else {
                  if ( image )
                    return cb( null, image );
                  else {
                    sails.log.error('image with id', id, 'not found!');
                    return cb ( true );
                  }
                }
              });
            },
            function( image, cb ) {
              ImageService.download({ 
                fileDescriptor: image.fileDescriptor,
                callback: function( err, file ) {
                  if ( err ) {
                    sails.log.error('failed to retrieve image', err );
                  }
                  return cb( null, image, file );
                }
              });
            }
          ];

      async.waterfall( actions, function( err, image, file ) {

        if ( err ) {
          return res.json({
            status: 500,
            message: lodash.template('could not retrieve image with id ${id}')({id: id}),
            data: err
          });
        }
        else {
          // Make image downloadable if param 'download' is true
          if ( download )
            res.setHeader('Content-disposition', 'attachment; filename=' + image.fileName);
          res.setHeader('Content-type', mime.lookup( image.fileName ) );
          return res.send( new Buffer( file ) );
        }
      });
    }

    // Upload image and associate it to model
    else if ( req.method.toUpperCase() === 'POST' && req.session.authenticated ) {

      var image = req.file('image'),
          id = req.param('id'),
          owner = req.param('owner'),
          description = req.param('description');

      if ( lodash.isEmpty( image ) || lodash.isEmpty( id ) || lodash.isEmpty( owner ) ) {
        return res.json({
          status: 500,
          message: 'error',
          data: 'image, id or owner fields empty'
        });
      }
      else {

        var actions = [

          function( cb ) {

            var handler = function( err, owner ) {
              if ( err ) {
                sails.log.error('Could not retreive band with id', id );
                return cb( err );
              }
              else {
                return cb( null, owner );
              }
            };

            switch( owner ) {
              case 'band':
                Band.findOne( id ).exec( handler );
                break;
              case 'album':
                Album.findOne( id ).exec( handler );
                break;
              case 'event':
                Event.findOne( id ).exec( handler );
                break;
              case 'venue':
                Venue.findOne( id ).exec( handler );
                break;
              default:
                return cb( 'unknown owner', null );
                break;
            }
          },

          function( owner, cb ) {

            var type;
            switch ( owner ) {
              case 'band':
                type = 'pictures';
                break;
              case 'album':
                type = 'covers';
                break;
              case 'event':
                type = 'photos';
                break;
              case 'venue':
                type = 'images';
                break;
              default:
                return cb( 'unknown owner', null, null );
                break;
            }
          
            ImageService.upload({
              image: image,
              identifier: owner,
              type: type,
              description: description,
              callback: function( err, results ) {
                if ( err ) {
                  sails.log.debug('erro uploading image', err );
                  return cb( err );
                }
                else {
                  return cb( null, owner, results );
                }
              }
            });
          }
        ];

        async.waterfall( actions, function( err, owner, results ) {
          if ( err ) {
            return res.json({
              status: 500,
              message: 'error',
              data: err
            });
          }
          else {
            return res.json({
              status: 200,
              message: 'success',
              data: results
            });
          }
        });       
      }
    }
    else {
      return res.json({
        status: ( req.session.authenticated ? 403 : 401 ),
        message: 'error',
        data: ( req.session.authenticated ? 'Action not allowed!' : 'Unauthorized action!')
      });
    }
  }
}
/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  profile: function( req, res, next ) {
    // Get user profile
    if ( req.method.toUpperCase() === 'GET' ) {

    }
    // Update user profile
    else if ( req.method.toUpperCase() === 'POST' ) {

    }
    else {
      return res.json({
        status: 403,
        message: 'Action not allowed!',
        data: null
      });
    }
  }
	
};


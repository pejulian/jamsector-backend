/**
* Event.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var Event = {

  attributes: {

    name: {
      type: 'string',
      minLength: 1,
      maxLength: 500,
      required: true
    },

    startDate: {
      type: 'date',
      required: true
    },

    endDate: {
      type: 'date',
      required: true
    },

    venue: {
      model: 'venue'
    },

    description: {
      type: 'string',
      minLength: 1,
      maxLength: 5000
    },

    location: {
      model: 'location'
    },

    photo: {
      model: 'image'
    },

    photos: {
      collection: 'image',
      via: 'photos'
    },

  },

  types: {
  }

};

module.exports = Event;


/**
* Venue.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var Venue = {

  attributes: {

    name: {
      type: 'string',
      minLength: 1,
      maxLength: 500,
      required: true
    },

    description: {
      type: 'string',
      minLength: 1,
      maxLength: 500
    },

    type: {
      type: 'string',
      enum: [
        'pub_bar_bistro', 
        'jamming_recording_studio'
      ]
    },

    location: {
      model: 'location'
    },

    image: {
      model: 'image'
    },

    images: {
      collection: 'image',
      via: 'images'
    },

    toJSON: function() {
      var obj = this.toObject();
      return obj;
    }
  },

  types: {

  }
};

module.exports = Venue;
/**
* Image.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

  	fileName: {
  		type: 'string',
  		required: true
  	},

  	fileDescriptor: {
  		type: 'string',
  		required: true
  	},

    format: {
      type: 'string'
    },

    size: {
      type: 'string'
    },

    description: {
      type: 'string',
      maxLength: 400
    },

    // Venue image
    image: {
      model: 'image'
    },

    // Venue images
    images: {
      model: 'venue',
      via: 'images'
    },

    // Event photo
    photo: {
      model: 'event'
    },

    photos: {
      model: 'event',
      via: 'photos'
    },

    // Album cover
    cover: {
      model: 'album'
    },

    covers: {
      model: 'album',
      via: 'covers'
    },

    // Band picture
    picture: {
      model: 'band'
    },

    pictures: {
      model: 'band',
      via: 'pictures'
    }
  },

  validation: {
    fileName: {
      required: 'File name is required!'
    },
    fileDescriptor: {
      required: 'File descriptor is required!'
    },
    description: {
      maxLength: 'Description must not exceed 400 characters!'
    }
  }
};


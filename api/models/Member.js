/**
* Member.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var Member = {

  attributes: {

    firstName: {
      type: 'string',
      minLength: 1,
      maxLength: 50
    },

    lastName: {
      type: 'string',
      minLength: 1,
      maxLength: 50
    },

    phoneNumber: {
      type: 'string',
      defaultsTo: '60123456789'
    },

    account   : { 
      type: 'string', 
      required: true, 
      enum: ['limited', 'full', 'admin', 'superuser']
    },

    role: {
      type: 'string'
    },

    user: {
      model: 'user'
    },

    fullName: function() {
      return this.firstName + ' ' + this.lastName;
    },

    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  },

  validation: {
    
    firstName: {
      minLength: 'First name must contain at least 1 characters!',
      maxLength: 'First name cannot exceed 50 characters!'
    },

    lastName: {
      minLength: 'Last name must contain at least 1 characters!',
      maxLength: 'Last name cannot exceed 50 characters!'
    }
  }
};

module.exports = Member;

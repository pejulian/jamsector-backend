/**
* Band.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

  	name: {
  		type: 'string',
  		minLength: 1,
  		maxLength: 500,
  		required: true
  	},

  	picture: {
      model: 'image'
  	},

    pictures: {
      collection: 'image',
      via: 'pictures'
    },

  	email: {
  		type: 'email',
  		required: true,
  		maxLength: 150
  	},

  	phone: {
  		type: 'string',
  		defaultsTo: '+60123456789',
  		maxLength: 30
  	},

  	address: {
  		type: 'string',
  		maxLength: 200
  	},

  	about: {
  		type: 'string',
      maxLength: 4000
  	},

  	formed: {
  		type: 'date',
  		required: true
  	},

  	toJSON: function() {
  		var obj = this.toObject();
  		return obj;
  	}
  },

  validation: {

    name: {
      minLength: 'Band name should contain at least 1 character!',
      maxLength: 'Band name should not exceed 500 characters!',
      required: 'A band name is required!'
    },

    email: {
      required: 'An email address is required!',
      maxLength: 'Email should not excced 150 characters!',
      email: 'Value should be a proper email'
    },

    phone: {
      maxLength: 'Phone number should not exceed 30 digits!'
    },

    address: {
      maxLength: 'Email should not excced 300 characters!'
    },

    formed: {
      required: 'Please enter the date when the band was created!',
      date: 'Value should be a ISO date string'
    } 
  }
};



var Location = {

  attributes: {

    coordinates: {
      type: 'json',
      point: true
    }

  },

  types: {
    point: function ( point ) {
      return point.x && point.y;
    },
  }
};

module.exports = Location;
/**
* Album.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var Album = {

  cover: {
    model: 'image'
  },

  covers: {
    collection: 'image',
    via: 'covers'
  },

  band: {
    model: 'band',
    required: true
  },

  arranger: {
    type: 'string'
  },

  date: {
    type: 'date',
    required: true
  },

  genre: {
    model: 'genre'
  },

  language: {
    type: 'string',
    defaultsTo: 'english'
  },

  producer: {
    type: 'string'
  },

  type: {
    type: 'string'
  },

  label: {
    model: 'label'
  },

  nationality: {
    type: 'string'
  },

  location: {
    model: 'location'
  }

};

module.exports = Album;



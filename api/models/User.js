var User = {
  // Enforce model schema in the case of schemaless databases
  schema: true,

  attributes: {
    username  : { type: 'string', unique: true, required: true },
    email     : { type: 'email',  unique: true, required: true },
    member    : { model: 'member', via: 'user' },
    passports : { collection: 'Passport', via: 'user' }
  },

  validation: {
    username: {
      unique: 'Username is not available!',
      required: 'Username is required!'
    },
    email: {
      unique: 'Email address already exists!',
      required: 'Email address is required!'
    }
  }
};

module.exports = User;

var Activity = {

  schema: true,

  attributes: {
    user    : { type: 'string', required: true },
    band    : { type: 'string', required: true },
    action  : { type: 'string', required: true, enum: ['follow', 'like'] }
  }

};

module.exports = Activity;
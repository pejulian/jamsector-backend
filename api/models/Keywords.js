
var Keywords = {

  keyword: {
    type: 'string',
    required: true
  },

  filter: {
    type: 'string',
    required: true
  }

};

module.exports = Keywords;
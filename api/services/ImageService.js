var lodash = require('lodash'),
    skipper = require('skipper-gridfs'),
    fs = require('fs'),
    lwip = require('lwip');

module.exports = {

  upload: function( options ) {

    var image = options.image,
        identifier = options.identifier,
        type = options.type,
        description = options.description,
        jamsectorDb = sails.config.connections.jamsectorDb,
        connectionString = lodash.template('http://${host}:${port}/${database}')({
          host: jamsectorDb.host,
          port: jamsectorDb.port,
          database: jamsectorDb.database
        });

    image.upload({
      adapter: skipper,
      uri: connectionString
    }, 
    function( err, uploadedFiles ) {
      if ( err ) {
        sails.log.error( err );
        options.callback( err );
      }

      var imagePromiseArray = [];
      lodash.each( uploadedFiles, function( file ) {

        var imageParams = {
          fileName: file.filename,
          fileDescriptor: file.fd,
          format: file.type,
          size: file.size
        };

        imageParams[type] = identifier.id;
        imageParams.description = description || file.filename;
        
        imagePromiseArray.push( Image.create( imageParams ) );
      });

      Promise.all( imagePromiseArray )
        .then( function( results ) {
          options.callback( null, results );
        })
        .catch( function( err ) {
          sails.log.error( err );
          options.callback( err );
      });
    });
  },

  download: function( options ) {

    var fileDescriptor = options.fileDescriptor,
        jamsectorDb = sails.config.connections.jamsectorDb,
        connectionString = lodash.template('http://${host}:${port}/${database}')({
          host: jamsectorDb.host,
          port: jamsectorDb.port,
          database: jamsectorDb.database
        });

    var adapter = skipper({
          uri: connectionString
        }).read( fileDescriptor, function( err, file ) {
          if ( err )
            return options.callback( err );
          else {

            // var buffer = new Buffer( file );
            // lwip.open( file, function( err, photo ) {
            //   if ( err )
            //     sails.log.error( err );
            // });

            return options.callback( null, file );
          }
        });
  },

  resize: function( options ) {

    var image = options.image;

    lwip.open( image, function( err, photo ) {
      if ( err ) {
        sails.log.error('error opening image', err );
        return options.callback( err, image );
      }
      else {
        sails.log.info('photo', photo );
      }
    });
  }
}
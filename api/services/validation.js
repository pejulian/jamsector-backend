/**
 * Takes a Sails Model object (e.g. User) and a ValidationError object and translates it into a friendly
 * object for sending via JSON to client-side frameworks.
 *
 * To use add a new object on your model describing what validation errors should be translated:
 *
 * module.exports = {
 *   attributes: {
 *     name: {
 *       type: 'string',
 *       required: true
 *     }
 *   },
 * 
 *   validation_messages: {
 *     name: {
 *       required: 'you have to specify a name or else'
 *     }
 *   }  
 * };
 *
 * Then in your controller you could write something like this:
 *
 * var validator = require('sails-validator-tool');
 *
 * Mymodel.create(options).done(function(error, mymodel) {
 *   if(error) {
 *     if(error.ValidationError) {
 *       error_object = validator(Mymodel, error.Validation);
 *       res.send({result: false, errors: error_object});
 *     }
 *   }
 * });
 *
 * @param model {Object} An instance of a Sails.JS model object.
 * @param validationErrors {Object} A standard Sails.JS validation object.
 *
 * @returns {Object} An object with friendly validation error conversions.
 */ 

var lodash = require('lodash');

var validation = function( model, validationError ) {

    var response = {},
        messages = model.validation;

    var fields = Object.keys(messages);
    lodash.forEach(fields, function( field ) {
      if ( validationError[field] ) {
        var processField = validationError[field];
        lodash.forEach( processField, function( rule ) {
            if ( messages[field][rule.rule] ) {
              if ( !response[field] ) 
                response[field] = [];
              response[field].push( messages[field][rule.rule] );
            }
        });
      }
    });
  
    return response;
};

module.exports = validation;

var lodash = require('lodash');

module.exports = {

  store: function( options ) {

    var keyword = options.keyword,
        filter = options.filter;

    if ( lodash.isEmpty( keyword ) || lodash.isNull( keyword ) ) {
      sails.log.warn('keyword is empty or undefined, skip keyword check');
      return;
    }

    if ( lodash.isEmpty( filter ) || lodash.isNull( filter ) ) {
      sails.log.warn('keyword filter is empty or undefined, skip keyword check');
      return;
    }

    keyword = lodash.trim( keyword );
    keyword = lodash.unescape( keyword );
    keyword = keyword.toUpperCase();

    Keywords
      .findOrCreate({
        keyword: keyword,
        filter: filter
      })
      .exec( function( err, keywords ) {
        if ( err ) {
          sails.log.error('an error occured during keyword lookup', err );
          return;
        }

        if ( lodash.isArray( keywords ) ) {
          sails.log.debug('keyword', keyword, 'exists, skip creating it');
          return;
        }

        if ( lodash.isObject( keywords ) ) {
          sails.log.debug('keyword', keyword, 'added...');
          return;
        }


      });

  }

}
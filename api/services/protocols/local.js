var validator = require('validator'),
    crypto    = require('crypto'),
    lodash    = require('lodash');

/**
 * Local Authentication Protocol
 *
 * The most widely used way for websites to authenticate users is via a username
 * and/or email as well as a password. This module provides functions both for
 * registering entirely new users, assigning passwords to already registered
 * users and validating login requesting.
 *
 * For more information on local authentication in Passport.js, check out:
 * http://passportjs.org/guide/username-password/
 */

/**
 * Register a new user
 *
 * This method creates a new user from a specified email, username and password
 * and assign the newly created user a local Passport.
 *
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
exports.register = function ( req, res, next ) {

  var email    = req.headers.email, 
      username = req.headers.username, 
      password = req.headers.password;

  UserService.create({
    email: email,
    username: username,
    password: password,
    callback: next
  });
};

/**
 * Assign local Passport to user
 *
 * This function can be used to assign a local Passport to a user who doens't
 * have one already. This would be the case if the user registered using a
 * third-party service and therefore never set a password.
 *
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
exports.connect = function ( req, res, next ) {

  var user     = req.user, 
      password = req.param('password');

  Passport.findOne({
    protocol : 'local', 
    user     : user.id
  }, function (err, passport) {

    if (err) {
      return next( err );
    }

    if (!passport) {
      Passport.create({
        protocol : 'local', 
        password : password, 
        user     : user.id
      }, function (err, passport) {
        next(err, user);
      });
    }
    else {
      next(null, user);
    }
  });
};

/**
 * Validate a login request
 *
 * Looks up a user using the supplied identifier (email or username) and then
 * attempts to find a local Passport associated with the user. If a Passport is
 * found, its password is checked against the password supplied in the form.
 *
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
exports.login = function ( req, res, next ) {

  var identifier = req.headers.identifier,
      password = req.headers.password,
      isEmail = validator.isEmail( identifier ), 
      query   = {};

  if ( isEmail )
    query.email = identifier;
  else
    query.username = identifier;

  User.findOne( query, function ( err, user ) {

    if ( err )
      return next( err );

    if ( !user ) {
      if ( isEmail ) {
        sails.log.error('User with email', query.email, 'not found!');
      }
      else {
        sails.log.error('User with username', query.username, 'not found!');
      }
      return next('User not found', false );
    }

    Passport
      .findOne({ protocol: 'local', user: user.id })
      .exec( function ( err, passport ) {

        if ( passport ) {

          if ( !password )
            return next('Missing password', false );

          passport.validatePassword( password, function (err, res) {

            if ( err ) {
              sails.log('err', err );
              return next( err );
            }
            
            if (!res) {
              sails.log.error('Incorrect password');
              return next('Incorrect password', false );
            } 
            else {
              return next( null, user );
            }
          });
        }
        else {
          sails.log.error('Missing passport');
          return next('Missing passport', false );
        }
      });
  });
};

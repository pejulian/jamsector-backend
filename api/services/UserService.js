var crypto    = require('crypto'),
    lodash    = require('lodash');

module.exports = {

  create: function( options ) {

  var email    = options.email, 
      username = options.username, 
      password = options.password,
      account  = options.account || 'limited';

    // Check if user exists, return the existing user if exists
    User
      .findOne()
      .where({ or: [{ username: username }, { email: email }] })
      .exec( function( err, user ) {

        // Error finding user
        if ( err ) {
          sails.log.error('An error occured looking up username', username, 'with email', email );
          return options.callback( err, null );
        }
        else {

          // User does not exist and will be created
          if ( lodash.isUndefined( user ) 
                || lodash.isNull( user ) 
                || lodash.isEmpty( user ) ) {

            User.create({
              username: username,
              email: email
            })
            .exec( function( err, user ) {

               if ( err ) {
                  if ( err.ValidationError )
                    err = validation( User, err.ValidationError );
                  return options.callback( err );
                }
                else {

                  // Create a member
                  Member
                    .create({
                      user: user.id,
                      account: account,
                    })
                    .exec( function( err, member ) {

                      // If member creation errored out, delete the user
                      if ( err ) {
                          
                        if ( err.ValidationError )
                          err = validation( Member, err.ValidationError );

                        User.destroy({ id: user.id }).exec( function( destroyErr ) {
                          return options.callback( destroyErr || err );
                        });
                      }
                      else {

                        // Associate the user to to the newly created member
                        User.update({ id: user.id }, { member: member.id }, function( err, updatedUser ) {
                          
                          // If user update fails, delete the user
                          if ( err ) {
                              
                            if ( err.ValidationError )
                              err = validation( User, err.ValidationError );

                            User.destroy({ id: user.id }).exec( function( destroyErr ) {
                              return options.callback( destroyErr || err );
                            });
                          }
                          else {

                            // Destroy any passport linked to the user
                            Passport.destroy({ user: user.id })
                              .exec( function( deztroyErr ) {

                                if ( deztroyErr )
                                  sails.log.error('failed to destroy passport for user'. user.id, err );
                               
                                // Generating accessToken for API authentication
                                var token = crypto.randomBytes(48).toString('base64');

                                // Create new passport
                                Passport.create({
                                  protocol    : 'local', 
                                  password    : password, 
                                  user        : user.id, 
                                  accessToken : token
                                })
                                .exec( function ( err, passport ) {

                                  // If fail to create passport, delete user record
                                  if ( err ) {
                                    
                                    if ( err.ValidationError )
                                      err = validation( Passport, err.ValidationError );

                                    User.destroy({ id: user.id }).exec( function( destroyErr ) {
                                      return options.callback( destroyErr || err );
                                    });
                                  }

                                  else {
                                    return options.callback( null, user );
                                  }
                                });
                              });
                          }
                        });
                      }
                    });
                }
            });
          }

          // Existing user
          else {
            sails.log.debug('UserService.create:', user.username, 'already exists ...');
            return options.callback( null, user );
          }
        }
    });
  }
}